﻿using ExcelExportAndImport.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ExcelExportAndImport.Extensions
{
    public static class ExcelPackageExtensions
    {
        public static DataTable ToDataTable(this ExcelPackage package)
        {
            ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
            //List<VinViewModel> list = new List<VinViewModel>();
            DataTable dt = new DataTable();
            foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
            {
                dt.Columns.Add(firstRowCell.Text);
            }
            for (int rowNumber = 2; rowNumber < workSheet.Dimension.End.Row; rowNumber++)
            {
                var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                var newRow = dt.NewRow();
                foreach (var cell in row)
                {
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
                dt.Rows.Add(newRow);
            }
            return dt;
        }
        public static List<VinViewModel> ToList(this ExcelPackage package)
        {
            ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
            List<VinViewModel> list = new List<VinViewModel>();
            VinViewModel model;
            //DataTable dt = new DataTable();
            //foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
            //{
            //    dt.Columns.Add(firstRowCell.Text);
            //}
            for (int rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
            {
                var row = workSheet.Cells[rowNumber, workSheet.Dimension.End.Column];

                foreach (var cell in row)
                {
                    model = new VinViewModel { Vin = cell.Text };
                    list.Add(model);

                }

            }
            return list;
        }
    }
}