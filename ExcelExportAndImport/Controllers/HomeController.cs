﻿using ExcelExportAndImport.Extensions;
using ExcelExportAndImport.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcelExportAndImport.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult DownloadHelp()
        {

            string path = AppDomain.CurrentDomain.BaseDirectory + "DownloadFiles/";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + "download.docx");
            string fileName = "download.docx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

        }

        public ActionResult ReadExcelUsingEpplus()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ReadExcel(HttpPostedFileBase upload)
        {
            if (Path.GetExtension(upload.FileName)==".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                //DataTable dt = ExcelPackageExtensions.ToDataTable(package);
                List<VinViewModel> list = ExcelPackageExtensions.ToList(package);
            }
            return View("ReadExcelUsingEpplus");
        }

        [HttpPost]
        public JsonResult ReadExcelWithAjax()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        ExcelPackage package = new ExcelPackage(file.InputStream);
                        List<VinViewModel> list = ExcelPackageExtensions.ToList(package);
                        Session["VinList"]= list;
                        //// Checking for Internet Explorer  
                        //if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        //{
                        //    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        //    fname = testfiles[testfiles.Length - 1];
                        //}
                        //else
                        //{
                        //    fname = file.FileName;
                        //}

                        //// Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        //file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
    }
}